import React from "react";
import "./App.css";
import Button from "./components/button/button";

function App() {
  return (
    <div className="App">
      <h1>Welcome iBOS PWA Testing</h1>
      <p>My React Test of PWA</p>
      <div>
        <Button label="CHECK Testing" />
      </div>
    </div>
  );
}

export default App;
