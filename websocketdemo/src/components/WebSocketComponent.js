import React, { Component } from "react";
import Websocket from "react-websocket";
import "./chat-list.css";

class WebSocketComponent extends Component {
  state = {
    messageList: [
      { message: "Hello Akash" },
      { message: "Hi Man, what about you ?" },
      { message: "Yes, what about you too ?" }
    ]
  };

  /**
   * handleData
   * Manage data when data received from websocket
   */
  handleData = messageData => {
    const data = JSON.parse(messageData);

    let messageList = this.state.messageList;
    if (data.length > 0) {
      messageList.push(data[0]);
      this.setState({
        messageList
      });
    }
  };

  render() {
    return (
      <div className="message-area">
        <ul className="list">
          {this.state.messageList.map((item, index) => (
            <li
              className={index % 2 === 0 ? "list-item-left" : "list-item-right"}
              key={index}
            >
              {item.message}
            </li>
          ))}
        </ul>
        <Websocket
          url="ws://127.0.0.1:9999/?topic=test&consumerGroup=group1&offset=1"
          onMessage={this.handleData}
        />
      </div>
    );
  }
}

export default WebSocketComponent;
